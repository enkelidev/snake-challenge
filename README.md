# Snake Challenge

## Description
The goal is to workout coding skills in general and learn / sharpen programming languages knowledge by creating a very simple snake game that runs on the terminal.

## Roadmap
Create the game in the following languages:
- [ ] Go
- [ ] Typescript
- [ ] Rust
- [ ] C#
- [ ] Python
- [ ] PHP
- [ ] C++
- [ ] Java

## License
see LICENSE.txt
