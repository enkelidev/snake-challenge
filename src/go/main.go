package main

import (
	"os"
	"os/exec"
)

func main() {
	channel := make(chan string)
	go func(channel chan string) {
		// disable input buffering
		exec.Command("stty", "-F", "/dev/tty", "cbreak", "min", "1").Run()
		// do not display entered characters on the screen
		exec.Command("stty", "-F", "/dev/tty", "-echo").Run()
		var b []byte = make([]byte, 1)
		for {
			os.Stdin.Read(b)
			channel <- string(b)
		}
	}(channel)

	ladder := false
	mode := ""

	for {
		stdin := <-channel
		switch stdin {
		case "A":
			mode = defaultString(ladder, "up", "")
		case "B":
			mode = defaultString(ladder, "down", "")
		case "C":
			mode = defaultString(ladder, "right", "")
		case "D":
			mode = defaultString(ladder, "left", "")
		case "[":
			ladder = true
			mode = ""
		default:
			ladder = false
			mode = ""
		}

		if len(mode) > 0 {
			println(mode)
		}

	}
}

func defaultString(condition bool, value string, defaultValue string) string {
	if condition {
		return value
	} else {
		return defaultValue
	}
}
